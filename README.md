# Svenska Brevet Tider

This is a web application for displaying Swedish randonneuring brevet times.

A live version is currently available here:
[Svenska Brevet Tider](https://randonneuring.gitlab.io/svenska-brevet-tider/)

The source for the brevet times themselves has been taken from the official
Swedish randonneuring organization:
[Randonneurs Sverige](http://www.randonneurs.se/Resultat.htm).

Screenshot:
![screenshot](screenshots/2020-11-04_screenshot.png)
