/*global $*/
'use strict';


// External libraries
var DATA_DISPLAY,

    // Global variables
    DATA_LOAD =
        {
            // The relative location of the data files
            dataSource : {
                "2021" : "data/formatted/spreadsheet_2021.csv",
                "2020" : "data/formatted/spreadsheet_2020.csv",
                "2019" : "data/formatted/spreadsheet_2019.csv",
                "2018" : "data/formatted/spreadsheet_2018.csv",
                "2017" : "data/formatted/spreadsheet_2017.csv",
                "2016" : "data/formatted/spreadsheet_2016.csv",
                "2015" : "data/formatted/spreadsheet_2015.csv",
                "2014" : "data/formatted/spreadsheet_2014.csv",
                "2013" : "data/formatted/spreadsheet_2013.csv",
                "2012" : "data/formatted/spreadsheet_2012.csv",
                "2011" : "data/formatted/spreadsheet_2011.csv",
                "2010" : "data/formatted/spreadsheet_2010.csv",
                "2009" : "data/formatted/spreadsheet_2009.csv",
                "2008" : "data/formatted/spreadsheet_2008.csv",
                "2007" : "data/formatted/spreadsheet_2007.csv",
                "2006" : "data/formatted/spreadsheet_2006.csv",
                "2005" : "data/formatted/spreadsheet_2005.csv",
                "2004" : "data/formatted/spreadsheet_2004.csv",
                "2003" : "data/formatted/spreadsheet_2003.csv",
            },

            // The dictionary array into which all loaded data will be placed
            brevetObjectArray : {},
            response : [],

            // Send an ajax request
            ajaxRequest : function (url) {

                var debug = false;

                // Send an ajax request to the server - the return value is the
                // response
                return $.ajax({

                    // Add version number url
                    url: url + "?v=202109272017",

                    success: function (response) {
                        if (debug) {
                            console.debug("AJAX " + url + " request success");
                            console.debug(response);
                        }
                    },

                    error: function (x, status, error) {
                        // Unauthorized access error
                        if (x.status === 403) {
                            console.log(
                                'HTTP 403: Forbidden (Access is not permitted)'
                            );
                        // Other errors
                        } else {
                            console.log("An error occurred: " + status);
                            console.log("Error: " + error);
                        }
                    },

                    // Settings used in all ajax requests
                    type: "GET",
                    dataType: "text",
                    async: true,
                    cache: true,
                    timeout: 20000
                });

            },

            // Parse the data taken from a csv file
            csvDataParse : function (response, year) {

                var debug = false, data, categories, i, j,
                    lastname,
                    firstname,
                    club,
                    place,
                    distance,
                    date,
                    time,
                    timeSplit,
                    timeHours,
                    regnr,
                    markerSize,
                    opacity,
                    hoverText = '',
                    numPeople,
                    brevetObject = {},
                    brevetObjectArray = [],
                    countParticipants = {};


                // Use a nice jquery function to read in comma seperated values
                // from text files
                data = $.csv.toArrays(response);
                if (debug) {
                    console.log(data);
                }

                // The data categories are in the first row of each file
                categories = data[0];
                if (debug) {
                    console.log(categories);
                }

                // Loop over data, save into arrays, massage a bit.
                // This assumes data is in a particular format an order - might
                // want to do somthing smarter
                for (i = 1; i < data.length; i += 1) {

                    // Trim excess left & right white space on input:
                    for (j = 0; j < data[i].length; j += 1) {
                        data[i][j] = String(data[i][j]).trim();
                    }

                    // The raw data, assumed to come in the same order and
                    // number for each data file - should add a check for
                    // this, or do something smarter like automtic population
                    // of fields based on the categories listed in the text
                    // files
                    lastname = data[i][0];
                    firstname = data[i][1];
                    club = data[i][2];
                    place = data[i][3];
                    distance = data[i][4];
                    date = data[i][5];
                    time = data[i][6];
                    regnr = data[i][7];

                    // Remove 'km' from distance along with extra white space
                    distance = distance.replace('km', '');
                    distance = distance.replace(/\s+/g, '');

                    // Convert differnt input time formats to HH:MM
                    if (time.includes("h")) {
                        time = time.replace("h", ":");
                    }
                    if (time.length === 4 && time.includes(":")) {
                        time = "0" + time;
                    }

                    // Convert time (hours, minutes) to just hours
                    if (time.includes(":")) {
                        timeSplit = time.split(":");
                        timeHours = (+timeSplit[0]) + (+timeSplit[1]) / 60;
                    } else {
                        timeHours = Number.NaN;
                    }

                    // For now, ignore items with no time value, e.g. Flèche
                    // rides - though some list the time as 24 hours, so ignore
                    // those too - for now anyways
                    if (!isNaN(timeHours) && !place.includes("Flèche")) {

                        // Some additional items
                        hoverText = String('<b>' + time + '</b>' + '<br><b> ',
                            firstname, ' ', lastname, '</b> - <i>', club,
                            '</i><br> ', place, ' ', date);
                        markerSize = 7;
                        opacity = 0.7;

                        // Add the all the data items to the brevet object
                        brevetObject = {
                            "lastname" : lastname,
                            "firstname" : firstname,
                            "club" : club,
                            "place" : place,
                            "distance" : distance,
                            "date" : date,
                            "time" : time,
                            "regnr" : regnr,
                            "timeHours" : timeHours,
                            "hoverText" : hoverText,
                            "opacity" : opacity,
                            "markerSize" : markerSize,
                            "year" : year,
                        };

                        // Keep a running total of the number of partipants
                        // in each brevet, which will have unique:
                        //  - date
                        //  - place
                        //  - distance
                        countParticipants =
                            DATA_LOAD.countParticipants(
                                date,
                                place,
                                distance,
                                countParticipants
                            );

                        // Add the brevet object to the brevet object array
                        brevetObjectArray.push(brevetObject);
                    }
                }

                // Loop through all the data again and set the number of
                // participants in each brevet
                for (i = 0; i < brevetObjectArray.length; i += 1) {

                    date = brevetObjectArray[i].date;
                    place = brevetObjectArray[i].place;
                    distance = brevetObjectArray[i].distance;

                    numPeople = 0;

                    // Double check that thiss array element exists
                    if (countParticipants.hasOwnProperty(date)) {
                        if (countParticipants[date].hasOwnProperty(place)) {
                            if (countParticipants[date][place].hasOwnProperty(
                                    distance
                                )) {
                                numPeople =
                                    countParticipants[date][place][distance];
                            }
                        }
                    }

                    brevetObjectArray[i].numPeople = numPeople;

                }

                return brevetObjectArray;
            },


            // Keep a running total of the numebr of partipants
            // in each brevet, which will have unique:
            //  - date
            //  - place
            //  - distance
            countParticipants : function (date, place, distance,
                countParticipants) {

                var debug = false;

                if (debug) {
                    console.log('date:            ' + date);
                    console.log('place:           ' + place);
                    console.log('distance:        ' + distance);
                    console.log(countParticipants);
                }

                if (countParticipants.hasOwnProperty(date)) {
                    if (countParticipants[date].hasOwnProperty(place)) {
                        if (countParticipants[date][place].hasOwnProperty(
                                distance
                            )) {
                            countParticipants[date][place][distance] += 1;
                        } else {
                            countParticipants[date][place][distance] = 1;
                        }
                    } else {
                        countParticipants[date][place] = {};
                        countParticipants[date][place][distance] = 1;
                    }
                } else {
                    countParticipants[date] = {};
                    countParticipants[date][place] = {};
                    countParticipants[date][place][distance] = 1;
                }

                return countParticipants;
            },


            // This function loops over all data files listed in
            // DATA_LOAD.dataSource, then when they are all read and parsed,
            // the plotting function is called.
            loadBrevetData : function () {

                var debug = false, dataFile, requests = [];

                if (debug) {
                    console.log("loadBrevetData called");
                }

                // Loop over each year and read the data file associated with
                // it
                Object.keys(DATA_LOAD.dataSource).forEach(
                    function (year, index) {

                        if (debug) {
                            console.log("year: ", year, ", index: ",
                                index);
                        }

                        // Read csv data, run as ajax request. Save output
                        // to a global variable to be used later.
                        dataFile = DATA_LOAD.dataSource[year];
                        requests.push(
                            $.when(DATA_LOAD.ajaxRequest(dataFile)).then(
                                function (response) {
                                    DATA_LOAD.response[year] = response;
                                }
                            )
                        );

                    }
                );

                // After all csv reading is done, send data to the plotting
                // functions, make some plots, and then load additional data
                // in the backgorund
                $.when.apply($, requests).then(
                    function () {

                        Object.keys(DATA_LOAD.response).forEach(
                            function (year) {
                                DATA_LOAD.brevetObjectArray[year] =
                                    DATA_LOAD.csvDataParse(
                                        DATA_LOAD.response[year],
                                        year
                                    );
                            }
                        );

                        if (debug) {
                            console.log("initial data reading jobs are " +
                                "complete");
                            console.log(DATA_LOAD.brevetObjectArray["2021"]);
                            console.log(DATA_LOAD.brevetObjectArray["2020"]);
                        }

                        // Populate the option lists
                        DATA_DISPLAY.populateOptionsLists();

                        // Filter data to be displayed
                        DATA_DISPLAY.filterData();

                        // Calculate simple stuff like total number and average
                        // in the plot
                        DATA_DISPLAY.calculateStatistics();

                        // Setup plotting object arrays
                        DATA_DISPLAY.fillDataTraces("0", "scattergl",
                            DATA_DISPLAY.plotData.xScatter,
                            DATA_DISPLAY.plotData.yScatter,
                            [], 1);
                        DATA_DISPLAY.fillDataTraces("1", "histogram",
                            DATA_DISPLAY.plotData.xHist, false,
                            DATA_DISPLAY.zoomRange.default.time, 0.25);
                        DATA_DISPLAY.fillDataTraces("2", "histogram",
                            DATA_DISPLAY.plotData.numParticpants, false,
                            DATA_DISPLAY.zoomRange.default.time, 4);

                        // Plot the data!
                        DATA_DISPLAY.drawPlots("0",
                            DATA_DISPLAY.zoomRange.selected.time,
                            false,
                            "Brevet Distans mot Tid",
                            "Tid (timmar)",
                            "Distans (km)",
                            DATA_DISPLAY.plotData.annotations["0"]);
                        DATA_DISPLAY.drawPlots("1",
                            DATA_DISPLAY.zoomRange.selected.time,
                            true,
                            "Nummer per 15 minuter",
                            "Tid (timmar)",
                            "Nummer",
                            DATA_DISPLAY.plotData.annotations["1"]);
                        DATA_DISPLAY.drawPlots("2",
                            DATA_DISPLAY.zoomRange.selected.numPeople,
                            true,
                            "Nummer av Deltagare per Brevet",
                            "Nummer av Deltagare",
                            "Nummer av Brevet",
                            DATA_DISPLAY.plotData.annotations["2"]);

                        // Turn off the loading spinner
                        document.getElementById("loader").style.display =
                                "none";
                    }
                );

            },

        };


// This function fires when the page is ready
$(document).ready(function () {

    var debug = false;

    if (debug) {
        console.log('document is ready');
    }

    // Load all the data and plot it
    DATA_LOAD.loadBrevetData();

});
